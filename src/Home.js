import React from "react";
import "./home.css";
import CardAccueil from "./asset/cardAccueil";

function Home() {
  return (
    <>
      <div className="home">
        <CardAccueil
          titre1="Nouvelle"
          titre2="Marque"
          format="large"
          image="/manteau.jpg"
        />
        <CardAccueil
          titre1="Nouvelle"
          titre2="Marque"
          format="large"
          image="/manteau.jpg"
        />
      </div>
    </>
  );
}

export default Home;
