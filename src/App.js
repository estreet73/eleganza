import "./App.css";
import { createContext, useState } from "react";
import Bandeau from "./asset/bandeau";
import LienModal from "./asset/lienModal";
import Recherche from "./asset/recherche";
import { Routes, Route } from "react-router-dom";
import Categorie from "./asset/categorie";
import Home from "./Home";
import ChaussuresFemmes from "./ChaussuresFemmes";
import VetementsFemmes from "./VetementsFemmes";
import ChaussuresHommes from "./ChaussuresHommes";
import VetementsHommes from "./VetementsHommes";
import Sale from "./Sale";
import Enfants from "./Enfants";
import Designers from "./Designers";
import Outlet from "./Outlet";
import Footer from "./asset/Footer";

export const context = createContext();
function App() {
  const [select, setSelect] = useState(0);
  const handleClose = () => {
    setSelect();
  };
  return (
    <div className="App">
      <context.Provider value={[select, setSelect, handleClose]}>
        <Bandeau fond={"#EBEBEB"} color={"#7A7A81"} />
        <LienModal />
        <Recherche />
        <Categorie />
      </context.Provider>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/sale" element={<Sale />} />
        <Route path="/chaussures_femmes" element={<ChaussuresFemmes />} />
        <Route path="/vetements_femmes" element={<VetementsFemmes />} />
        <Route path="/chaussures_hommes" element={<ChaussuresHommes />} />
        <Route path="/vetements_hommes" element={<VetementsHommes />} />
        <Route path="/enfants" element={<Enfants />} />
        <Route path="/designers" element={<Designers />} />
        <Route path="/outlet" element={<Outlet />} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
