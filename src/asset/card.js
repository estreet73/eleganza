import "./card.css";
import { useState } from "react";

function Card({ img1, img2, reduct, nom, description, tarif, tarif2 }) {
  const [img, setImg] = useState(img1);
  const handChange = () => {
    setImg(img2);
  };

  const handleInit = () => {
    setImg(img1);
  };
  return (
    <div className="bloc">
      <div className="card">
        {reduct ? (
          <div className="reduction">REDUCTION {`${reduct}`} %</div>
        ) : null}
        <img
          onMouseMove={handChange}
          onMouseLeave={handleInit}
          className="chaussure"
          height="300px"
          width="300px"
          src={img}
          alt="chaussure"
        />
      </div>
      <div className="descriptif">
        <span>{nom}</span>
        <span>{description}</span>
        <div>
          {tarif2 ? <span className="tarif2">{tarif2} </span> : null}
          <span style={reduct ? { color: "#44CBCB" } : { color: "black" }}>
            {tarif}
          </span>
        </div>
      </div>
    </div>
  );
}

export default Card;
