import "./ListeProduits.css";
import Card from "./card";

function ListeProduits() {
  return (
    <div className="bodyListe">
      <div className="filtre">FILTRE</div>
      <div className="produits">
        <Card
          img1="/chaussure.jpg"
          img2="/chaussure_profil.jpg"
          nom="VEJA"
          reduct="30"
          description="Unisexe Campo Chromefree Leather"
          tarif="130,00"
          tarif2="150,00"
        />
        <Card
          img1="/chaussure.jpg"
          img2="/chaussure_profil.jpg"
          nom="VEJA"
          description="Unisexe Campo Chromefree Leather"
          tarif="130,00"
        />
        <Card
          img1="/chaussure.jpg"
          img2="/chaussure_profil.jpg"
          nom="VEJA"
          description="Unisexe Campo Chromefree Leather"
          tarif="130,00"
        />
      </div>
    </div>
  );
}

export default ListeProduits;
