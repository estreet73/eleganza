import "./recherche.css";
import { useContext } from "react";
import { context } from "../App";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faEuroSign,
  faUser,
} from "@fortawesome/free-solid-svg-icons";

function Recherche() {
  const state = useContext(context);
  // eslint-disable-next-line no-unused-vars
  const [select, handleClose] = state;
  let navigate = useNavigate();

  const handleClick = (id) => navigate(id);
  return (
    <div onMouseMove={() => handleClose()} className="recherche">
      <div className="look">
        <div>
          <FontAwesomeIcon icon={faSearch} />
        </div>
        <input className="input" placeholder="Que recherchez vous ?"></input>
      </div>
      <div>
        <img
          onClick={() => handleClick("/")}
          height="25px"
          src="/logo.svg"
          alt="logo"
        />
      </div>

      <div className="icon">
        <div className="rond">
          <FontAwesomeIcon className="fa" icon={faEuroSign} />
        </div>
        <div className="rond"></div>
        <div className="rond">
          <FontAwesomeIcon className="fa" icon={faUser} />
        </div>
        <div className="rond"></div>
      </div>
    </div>
  );
}

export default Recherche;
