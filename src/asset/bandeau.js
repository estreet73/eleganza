import "./bandeau.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

function Bandeau({ fond, color }) {
  return (
    <div
      className="container"
      style={{ backgroundColor: `${fond}`, color: `${color}` }}
    >
      <div>
        <FontAwesomeIcon className="check" icon={faCheck} /> Livraisons rapides
        dans les 1-3 jours ouvrables
      </div>
      <div>
        <FontAwesomeIcon className="check" icon={faCheck} />
        Livraison gratuite en Europe!
      </div>
      <div>
        <FontAwesomeIcon className="check" icon={faCheck} />
        Garantie de remboursement! aussi sur les articles en vente
      </div>
    </div>
  );
}

export default Bandeau;
