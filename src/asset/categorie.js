import "./categorie.css";
import Menu from "./menu";
import { useEffect, useState, useContext } from "react";
import { context } from "../App";
import { useNavigate } from "react-router-dom";

function Categorie() {
  const theme = useContext(context);
  const [select, setSelect, handleClose] = theme;
  const [option, setOption] = useState();
  let navigate = useNavigate();
  const handleChange = (id) => {
    setSelect(id);
  };

  const handleClick = (id) => {
    navigate(`/${id}`);
  };

  useEffect(() => {
    setOption(select);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [select]);

  return (
    <>
      <div className="categorie">
        <div
          className="choix"
          onClick={() => handleClick("sale")}
          onMouseMove={() => handleChange(1)}
        >
          Sale
        </div>
        <div
          className="choix"
          onClick={() => handleClick("chaussures_femmes")}
          onMouseMove={() => handleChange(2)}
        >
          Chaussures pour femme
        </div>
        <div
          className="choix"
          onClick={() => handleClick("vetements_femmes")}
          onMouseMove={() => handleChange(3)}
        >
          Vetements pour femme
        </div>
        <div
          className="choix"
          onClick={() => handleClick("chaussures_hommes")}
          onMouseMove={() => handleChange(4)}
        >
          Chaussures pour homme
        </div>
        <div
          className="choix"
          onClick={() => handleClick("vetements_hommes")}
          onMouseMove={() => handleChange(5)}
        >
          Vetements pour homme
        </div>
        <div
          className="choix"
          onClick={() => handleClick("enfants")}
          onMouseMove={() => handleChange(6)}
        >
          Enfants
        </div>
        <div
          className="choix"
          onClick={() => handleClick("designers")}
          onMouseMove={() => handleChange(7)}
        >
          Designers
        </div>
        <div
          className="choix outlet"
          onClick={() => handleClick("outlet")}
          onMouseMove={() => handleChange(8)}
        >
          Outlet
        </div>
      </div>
      <div>
        <Menu select={option} handleClose={handleClose} />
      </div>
    </>
  );
}

export default Categorie;
