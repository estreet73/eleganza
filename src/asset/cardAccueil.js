import { useEffect, useState } from "react";
import "./cardAccueil.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltRight } from "@fortawesome/free-solid-svg-icons";

function CardAccueil({ titre1, titre2, titre3, image, format, valentino }) {
  const [up, setUp] = useState(false);
  const [scale, setScale] = useState("100% 100%");
  const [width, setWidth] = useState("300px");
  const [height, setHeight] = useState("500px");

  const handleUp = () => {
    setUp(true);
    setScale("103% 103%");
  };
  const handleDown = () => {
    setUp(false);
    setScale("100% 100%");
  };

  useEffect(() => {
    if (format === "large") {
      setWidth("630px");
    }
    if (format === "small") {
      setHeight("235px");
    }
  }, [format]);
  return (
    <div
      onMouseMove={handleUp}
      onMouseLeave={handleDown}
      className="contour"
      style={{
        backgroundImage: `url(${image})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: `${scale}`,
        display: "flex",
        height: `${height}`,
        width: `${width}`,
        overflow: "hidden",
        margin: "20px",
      }}
    >
      <div className="card-text">
        <p className="titre-card">{titre1} </p>
        <p className="titre-card">{titre2}</p>
        <p className="titre-card">{titre3}</p>
        {up ? (
          valentino ? (
            <p className="valentino">VALENTINO</p>
          ) : (
            <p className="achetez">
              achetez <FontAwesomeIcon icon={faLongArrowAltRight} />
            </p>
          )
        ) : null}
      </div>
    </div>
  );
}

export default CardAccueil;
