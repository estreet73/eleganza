import "./menu.css";
import { useContext } from "react";
import { context } from "../App";

function Menu() {
  const theme = useContext(context);
  const [select, handleClose] = theme;
  return (
    <>
      {select === 2 ? (
        <div onMouseLeave={() => handleClose()} className="menu">
          <div className="range1">
            <h3>Chaussures pour femme</h3>
            <p className="titre">Basckets</p>
            <p className="titre">Basckets</p>
            <p className="titre">Chaussures a lacet</p>
            <p className="titre">Mocassins & Ballerines</p>
            <p className="titre">Escarpins & Talons hauts</p>
            <p className="titre">Claquettes & sandales</p>
          </div>
          <div>
            <h3>Designers</h3>
            <p className="titre">Alaxanders McQueen</p>
            <p className="titre">Balenciaga</p>
            <p className="titre">Isabelle Marant</p>
            <p className="titre">Golden Goose</p>
            <p className="titre">Philippe Model</p>
          </div>
        </div>
      ) : null}
      {select === 3 ? (
        <div onMouseLeave={() => handleClose()} className="menu">
          <div className="range1">
            <h3>Vetements pour femme</h3>
            <p className="titre">Basckets</p>
            <p className="titre">Basckets</p>
            <p className="titre">Chaussures a lacet</p>
            <p className="titre">Mocassins & Ballerines</p>
            <p className="titre">Escarpins & Talons hauts</p>
            <p className="titre">Claquettes & sandales</p>
          </div>
          <div className="range1">
            <h3>Sacs & Accessoires</h3>
            <p className="titre">Basckets</p>
            <p className="titre">Basckets</p>
            <p className="titre">Chaussures a lacet</p>
            <p className="titre">Mocassins & Ballerines</p>
            <p className="titre">Escarpins & Talons hauts</p>
            <p className="titre">Claquettes & sandales</p>
          </div>
          <div>
            <h3>Designers</h3>
            <p className="titre">Alaxanders McQueen</p>
            <p className="titre">Balenciaga</p>
            <p className="titre">Isabelle Marant</p>
            <p className="titre">Golden Goose</p>
            <p className="titre">Philippe Model</p>
          </div>
        </div>
      ) : null}
    </>
  );
}

export default Menu;
